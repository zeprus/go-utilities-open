package com.zeprus.goutilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import eu.chainfire.libsuperuser.Shell;

public class SettingsActivity extends AppCompatActivity{
 TextView appninjasPackageDisplay;
 TextView discordCookiesDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        final SharedPreferences preferences = getSharedPreferences("settings", 0);
        final SharedPreferences.Editor editor = preferences.edit();
        final Context context = this;

        final Switch appninjasSupportSwitch = findViewById(R.id.switch_appninjas_teleport);
        final Switch coordinatesSupportSwitch = findViewById(R.id.switch_coordinates);
        final Switch communityLinkSupportSwitch = findViewById(R.id.switch_discord_community_uri);
        final Switch handleClipboardSwitch = findViewById(R.id.switch_handle_clipboard);
        final Switch autoTeleportSwitch = findViewById(R.id.switch_auto_teleport);
        final Switch autoLaunchSwitch = findViewById(R.id.switch_auto_launch);
        final Switch liveFeedSwitch = findViewById(R.id.switch_live_maps);
        final Switch nycLiveMapSwitch = findViewById(R.id.switch_nyc_map);
        final Switch sgLiveMapSwitch = findViewById(R.id.switch_sg_map);
        final Switch removeSnipedSwitch = findViewById(R.id.switch_remove_sniped);
        final Switch freshSpawns = findViewById(R.id.switch_fresh_spawns);
        final Switch rootSwitch = findViewById(R.id.switch_root_mode);

        final Button selectAppninjas = findViewById(R.id.button_select_appninjas);
        final Button setDiscord = findViewById(R.id.button_set_discord);

        appninjasPackageDisplay = findViewById(R.id.appninjas_package_display);
        discordCookiesDisplay = findViewById(R.id.discord_cookies_display);
        final View discordView = findViewById(R.id.layout_enter_discord);

        appninjasSupportSwitch.setChecked(preferences.getBoolean("supportAppninjas", false));
        coordinatesSupportSwitch.setChecked(preferences.getBoolean("supportCoordinates", false));
        communityLinkSupportSwitch.setChecked(preferences.getBoolean("supportCommunityURI", false));
        handleClipboardSwitch.setChecked(preferences.getBoolean("handleClipboard", false));
        autoTeleportSwitch.setChecked(preferences.getBoolean("supportAutoTeleport", false));
        autoLaunchSwitch.setChecked(preferences.getBoolean("autoLaunchGO", false));
        liveFeedSwitch.setChecked(preferences.getBoolean("liveFeed", false));
        nycLiveMapSwitch.setChecked(preferences.getBoolean("nycFeed", false));
        sgLiveMapSwitch.setChecked(preferences.getBoolean("sgFeed", false));
        removeSnipedSwitch.setChecked(preferences.getBoolean("removeSniped", false));
        freshSpawns.setChecked(preferences.getBoolean("freshSpawns", false));
        rootSwitch.setChecked(preferences.getBoolean("rootMode", false));

        if(!preferences.getBoolean("handleClipboard", false)){
            coordinatesSupportSwitch.setEnabled(false);
            communityLinkSupportSwitch.setEnabled(false);
            findViewById(R.id.layout_format_clipboard).setVisibility(View.GONE);
        }

        if(!preferences.getBoolean("supportAutoTeleport", false)){
            findViewById(R.id.layout_auto_teleport).setVisibility(View.GONE);
        }

        if(!preferences.getBoolean("supportAppninjas", false)){
            selectAppninjas.setEnabled(false);
        }

        if(!preferences.getBoolean("supportCommunityURI", false)){
            setDiscord.setEnabled(false);
            findViewById(R.id.community_link_layout).setVisibility(View.GONE);
        }

        if(!preferences.getBoolean("liveFeed", false)){
            findViewById(R.id.live_feed_layout).setVisibility(View.GONE);
        }

        appninjasPackageDisplay.setText(preferences.getString("packageNameAppninjas", "unset"));
        discordCookiesDisplay.setText(preferences.getString("discordCookies", "unset"));

        appninjasSupportSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("supportAppninjas", true);
                    selectAppninjas.setEnabled(true);

                } else {
                    editor.putBoolean("supportAppninjas", false);
                    selectAppninjas.setEnabled(false);
                }
                editor.apply();
            }
        });

        coordinatesSupportSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("supportCoordinates", true);
                } else {
                    editor.putBoolean("supportCoordinates", false);
                }
                editor.commit();
            }
        });

        communityLinkSupportSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    findViewById(R.id.community_link_layout).setVisibility(View.VISIBLE);
                    editor.putBoolean("supportCommunityURI", true);
                    setDiscord.setEnabled(true);
                } else {
                    editor.putBoolean("supportCommunityURI", false);
                    findViewById(R.id.community_link_layout).setVisibility(View.GONE);
                    setDiscord.setEnabled(false);
                    discordView.setVisibility(View.GONE);
                }
                editor.commit();
            }
        });

        autoLaunchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("autoLaunchGO", true);
                } else {
                    editor.putBoolean("autoLaunchGO", false);
                }
                editor.commit();
            }
        });

        handleClipboardSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("handleClipboard", true);
                    coordinatesSupportSwitch.setEnabled(true);
                    communityLinkSupportSwitch.setEnabled(true);
                    findViewById(R.id.layout_format_clipboard).setVisibility(View.VISIBLE);
                } else {
                    editor.putBoolean("handleClipboard", false);
                    coordinatesSupportSwitch.setEnabled(false);
                    coordinatesSupportSwitch.setChecked(false);
                    communityLinkSupportSwitch.setEnabled(false);
                    communityLinkSupportSwitch.setChecked(false);
                    findViewById(R.id.layout_format_clipboard).setVisibility(View.GONE);
                }
                editor.commit();
            }
        });
        autoTeleportSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("supportAutoTeleport", true);
                    findViewById(R.id.layout_auto_teleport).setVisibility(View.VISIBLE);
                } else {
                    editor.putBoolean("supportAutoTeleport", false);
                    findViewById(R.id.layout_auto_teleport).setVisibility(View.GONE);
                    appninjasSupportSwitch.setChecked(false);
                }
                editor.commit();
            }
        });
        liveFeedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("liveFeed", true);
                    findViewById(R.id.live_feed_layout).setVisibility(View.VISIBLE);
                } else {
                    editor.putBoolean("liveFeed", false);
                    findViewById(R.id.live_feed_layout).setVisibility(View.GONE);
                    nycLiveMapSwitch.setChecked(true);
                    sgLiveMapSwitch.setChecked(true);
                }
                editor.commit();
            }
        });
        nycLiveMapSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("nycFeed", true);
                } else {
                    editor.putBoolean("nycFeed", false);
                }
                editor.commit();
            }
        });
        sgLiveMapSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("sgFeed", true);
                } else {
                    editor.putBoolean("sgFeed", false);
                }
                editor.commit();
            }
        });
        removeSnipedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("removeSniped", true);
                } else {
                    editor.putBoolean("removeSniped", false);
                }
                editor.commit();
            }
        });
        freshSpawns.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    editor.putBoolean("freshSpawns", true);
                } else {
                    editor.putBoolean("freshSpawns", false);
                }
                editor.commit();
            }
        });

        rootSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    Log.d("Available: ", " " + Shell.SU.available());
                    if (Shell.SU.available()) {
                        editor.putBoolean("rootMode", true);
                    } else {
                        Toast toast = Toast.makeText(context, "Failed to get root access.", Toast.LENGTH_SHORT);
                        toast.show();
                        rootSwitch.setChecked(false);
                    }
                } else {
                    editor.putBoolean("rootMode", false);
                }
                editor.commit();
            }
        });
    }
    public void selectPackageName(View v) {
        Intent i = new Intent(SettingsActivity.this, AppListActivity.class);
        startActivity(i);
    }
    public void selectDiscordCookies(View v){
        View selectLabel = findViewById(R.id.layout_enter_discord);
        selectLabel.setVisibility(View.VISIBLE);
    }
    public void submitDiscordCookies(View v) {
        EditText inputText = findViewById(R.id.discord_label_input);
        String input = inputText.getText().toString();
        final SharedPreferences preferences = getSharedPreferences("settings", 0);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString("discordCookies", input);
        editor.apply();
        discordCookiesDisplay.setText(input);
        findViewById(R.id.layout_enter_discord).setVisibility(View.GONE);

    }
    public void abortDiscordCookie(View v) {
        View selectLabel = findViewById(R.id.layout_enter_discord);
        selectLabel.setVisibility(View.GONE);
    }
    public void setupPokemonPref(View v){
        Intent i = new Intent(this, PokemonPrefActivity.class);
        startActivity(i);
    }
}

