package com.zeprus.goutilities;

public class Pokemon {
    private int pokemon_id;
    private int cp;
    private int iv;
    private double x;
    private double y;
    private long despawn;
    boolean shiny;
    String name;

    public Pokemon(String name, int pokemon_id, double x, double y){
        //min constructor
        this.pokemon_id = pokemon_id;
        this.x = x;
        this.y = y;
        this.name = name;
    }
    public Pokemon(String name, int pokemon_id, double x, double y, long despawn, boolean shiny){
        //live map constructor
        this.pokemon_id = pokemon_id;
        this.x = x;
        this.y = y;
        this.despawn = despawn;
        this.shiny = shiny;
        this.name = name;
    }
    public Pokemon(String name, int pokemon_id, double x, double y, int cp, int iv){
        //pokedex100 constructor
        this.pokemon_id = pokemon_id;
        this.x = x;
        this.y = y;
        this.cp = cp;
        this.iv = iv;
        this.name = name;
    }
    public String getName(){ return this.name; }

    public int getID(){
        return pokemon_id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getCp() {
        return cp;
    }

    public int getIv() {
        return iv;
    }

    public long getDespawn() {
        return despawn;
    }

    public int getPokemon_id() {
        return pokemon_id;
    }

    public boolean isShiny() {
        return shiny;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public void setDespawn(long despawn) {
        this.despawn = despawn;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setShiny(boolean shiny) {
        this.shiny = shiny;
    }

    public void setPokemon_id(int pokemon_id) {
        this.pokemon_id = pokemon_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIv(int iv) {
        this.iv = iv;
    }
}
