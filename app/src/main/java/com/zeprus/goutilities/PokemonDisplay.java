package com.zeprus.goutilities;

public class PokemonDisplay {
    String name;
    String id;
    boolean enabled;

    public PokemonDisplay(String name, String id, boolean enabled){
        this.name = name;
        this.id = id;
        this.enabled = enabled;
    }

    public String getName(){
        return name;
    }
    public String getId(){
        return id;
    }
    public boolean isEnabled(){return enabled;}

    public void setName(String name) {
        this.name = name;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    public void setId(String id) {
        this.id = id;
    }
}
