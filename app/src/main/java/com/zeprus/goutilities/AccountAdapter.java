package com.zeprus.goutilities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AccountAdapter extends ArrayAdapter<Account> implements View.OnClickListener {

    private ArrayList<Account> accountArrayList;
    private Context context;

    AccountAdapter(ArrayList<Account> accountArrayList, Context context){
        super(context, R.layout.account_list_template, accountArrayList);
        this.accountArrayList = accountArrayList;
        this.context = context;
        PackageManager pm = context.getPackageManager();
        ApplicationInfo appInfo = null;
        try {
            appInfo = pm.getApplicationInfo("com.nianticlabs.pokemongo", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert appInfo != null;
    }

    private static class ViewHolder {
        TextView name;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final AccountAdapter.ViewHolder holder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.account_list_template, parent, false);
            holder = new AccountAdapter.ViewHolder();
            convertView.setTag(holder);
            holder.name = convertView.findViewById(R.id.account_list_template_name);
        }
        else {
            holder=(AccountAdapter.ViewHolder) convertView.getTag();
        }
        holder.name.setText(accountArrayList.get(position).name);
        if(accountArrayList.get(position).isCurrent()){
            holder.name.setBackgroundColor(ContextCompat.getColor(context, R.color.windowBackgroundDark));
        } else {
            holder.name.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }
        return convertView;
    }

    @Override
    public void onClick(View v) {

    }
}
