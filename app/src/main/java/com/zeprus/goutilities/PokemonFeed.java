package com.zeprus.goutilities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
//todo: Map view with locations
//todo: Display spawn/despawn time
//todo: pokemon icons
public class PokemonFeed extends AppCompatActivity {
    SharedPreferences preferences;
    String[] pokeNames ={"Bulbasaur","Ivysaur","Venusaur","Charmander","Charmeleon","Charizard","Squirtle","Wartortle","Blastoise","Caterpie","Metapod","Butterfree","Weedle","Kakuna","Beedrill","Pidgey","Pidgeotto","Pidgeot","Rattata","Raticate","Spearow","Fearow","Ekans","Arbok","Pikachu","Raichu","Sandshrew","Sandslash","NidoranF","Nidorina","Nidoqueen","NidoranM","Nidorino","Nidoking","Clefairy","Clefable","Vulpix","Ninetales","Jigglypuff","Wigglytuff","Zubat","Golbat","Oddish","Gloom","Vileplume","Paras","Parasect","Venonat","Venomoth","Diglett","Dugtrio","Meowth","Persian","Psyduck","Golduck","Mankey","Primeape","Growlithe","Arcanine","Poliwag","Poliwhirl","Poliwrath","Abra","Kadabra","Alakazam","Machop","Machoke","Machamp","Bellsprout","Weepinbell","Victreebel","Tentacool","Tentacruel","Geodude","Graveler","Golem","Ponyta","Rapidash","Slowpoke","Slowbro","Magnemite","Magneton","Farfetchd","Doduo","Dodrio","Seel","Dewgong","Grimer","Muk","Shellder","Cloyster","Gastly","Haunter","Gengar","Onix","Drowzee","Hypno","Krabby","Kingler","Voltorb","Electrode","Exeggcute","Exeggutor","Cubone","Marowak","Hitmonlee","Hitmonchan","Lickitung","Koffing","Weezing","Rhyhorn","Rhydon","Chansey","Tangela","Kangaskhan","Horsea","Seadra","Goldeen","Seaking","Staryu","Starmie","Mr.Mime","Scyther","Jynx","Electabuzz","Magmar","Pinsir","Tauros","Magikarp","Gyarados","Lapras","Ditto","Eevee","Vaporeon","Jolteon","Flareon","Porygon","Omanyte","Omastar","Kabuto","Kabutops","Aerodactyl","Snorlax","Articuno","Zapdos","Moltres","Dratini","Dragonair","Dragonite","Mewtwo","Mew","Chikorita","Bayleef","Meganium","Cyndaquil","Quilava","Typhlosion","Totodile","Croconaw","Feraligatr","Sentret","Furret","Hoothoot","Noctowl","Ledyba","Ledian","Spinarak","Ariados","Crobat","Chinchou","Lanturn","Pichu","Cleffa","Igglybuff","Togepi","Togetic","Natu","Xatu","Mareep","Flaaffy","Ampharos","Bellossom","Marill","Azumarill","Sudowoodo","Politoed","Hoppip","Skiploom","Jumpluff","Aipom","Sunkern","Sunflora","Yanma","Wooper","Quagsire","Espeon","Umbreon","Murkrow","Slowking","Misdreavus","Unown","Wobbuffet","Girafarig","Pineco","Forretress","Dunsparce","Gligar","Steelix","Snubbull","Granbull","Qwilfish","Scizor","Shuckle","Heracross","Sneasel","Teddiursa","Ursaring","Slugma","Magcargo","Swinub","Piloswine","Corsola","Remoraid","Octillery","Delibird","Mantine","Skarmory","Houndour","Houndoom","Kingdra","Phanpy","Donphan","Porygon2","Stantler","Smeargle","Tyrogue","Hitmontop","Smoochum","Elekid","Magby","Miltank","Blissey","Raikou","Entei","Suicune","Larvitar","Pupitar","Tyranitar","Lugia","Ho-Oh","Celebi","Treecko","Grovyle","Sceptile","Torchic","Combusken","Blaziken","Mudkip","Marshtomp","Swampert","Poochyena","Mightyena","Zigzagoon","Linoone","Wurmple","Silcoon","Beautifly","Cascoon","Dustox","Lotad","Lombre","Ludicolo","Seedot","Nuzleaf","Shiftry","Taillow","Swellow","Wingull","Pelipper","Ralts","Kirlia","Gardevoir","Surskit","Masquerain","Shroomish","Breloom","Slakoth","Vigoroth","Slaking","Nincada","Ninjask","Shedinja","Whismur","Loudred","Exploud","Makuhita","Hariyama","Azurill","Nosepass","Skitty","Delcatty","Sableye","Mawile","Aron","Lairon","Aggron","Meditite","Medicham","Electrike","Manectric","Plusle","Minun","Volbeat","Illumise","Roselia","Gulpin","Swalot","Carvanha","Sharpedo","Wailmer","Wailord","Numel","Camerupt","Torkoal","Spoink","Grumpig","Spinda","Trapinch","Vibrava","Flygon","Cacnea","Cacturne","Swablu","Altaria","Zangoose","Seviper","Lunatone","Solrock","Barboach","Whiscash","Corphish","Crawdaunt","Baltoy","Claydol","Lileep","Cradily","Anorith","Armaldo","Feebas","Milotic","Castform","Kecleon","Shuppet","Banette","Duskull","Dusclops","Tropius","Chimecho","Absol","Wynaut","Snorunt","Glalie","Spheal","Sealeo","Walrein","Clamperl","Huntail","Gorebyss","Relicanth","Luvdisc","Bagon","Shelgon","Salamence","Beldum","Metang","Metagross","Regirock","Regice","Registeel","Latias","Latios","Kyogre","Groudon","Rayquaza","Jirachi","Deoxys","Turtwig","Grotle","Torterra","Chimchar","Monferno","Infernape","Piplup","Prinplup","Empoleon","Starly","Staravia","Staraptor","Bidoof","Bibarel","Kricketot","Kricketune","Shinx","Luxio","Luxray","Budew","Roserade","Cranidos","Rampardos","Shieldon","Bastiodon","Burmy","Wormadam","Mothim","Combee","Vespiquen","Pachirisu","Buizel","Floatzel","Cherubi","Cherrim","Shellos","Gastrodon","Ambipom","Drifloon","Drifblim","Buneary","Lopunny","Mismagius","Honchkrow","Glameow","Purugly","Chingling","Stunky","Skuntank","Bronzor","Bronzong","Bonsly","MimeJr.","Happiny","Chatot","Spiritomb","Gible","Gabite","Garchomp","Munchlax","Riolu","Lucario","Hippopotas","Hippowdon","Skorupi","Drapion","Croagunk","Toxicroak","Carnivine","Finneon","Lumineon","Mantyke","Snover","Abomasnow","Weavile","Magnezone","Lickilicky","Rhyperior","Tangrowth","Electivire","Magmortar","Togekiss","Yanmega","Leafeon","Glaceon","Gliscor","Mamoswine","Porygon-Z","Gallade","Probopass","Dusknoir","Froslass","Rotom","Uxie","Mesprit","Azelf","Dialga","Palkia","Heatran","Regigigas","Giratina","Cresselia","Phione","Manaphy","Darkrai","Shaymin","Arceus","Victini","Snivy","Servine","Serperior","Tepig","Pignite","Emboar","Oshawott","Dewott","Samurott","Patrat","Watchog","Lillipup","Herdier","Stoutland","Purrloin","Liepard","Pansage","Simisage","Pansear","Simisear","Panpour","Simipour","Munna","Musharna","Pidove","Tranquill","Unfezant","Blitzle","Zebstrika","Roggenrola","Boldore","Gigalith","Woobat","Swoobat","Drilbur","Excadrill","Audino","Timburr","Gurdurr","Conkeldurr","Tympole","Palpitoad","Seismitoad","Throh","Sawk","Sewaddle","Swadloon","Leavanny","Venipede","Whirlipede","Scolipede","Cottonee","Whimsicott","Petilil","Lilligant","Basculin","Sandile","Krokorok","Krookodile","Darumaka","Darmanitan","Maractus","Dwebble","Crustle","Scraggy","Scrafty","Sigilyph","Yamask","Cofagrigus","Tirtouga","Carracosta","Archen","Archeops","Trubbish","Garbodor","Zorua","Zoroark","Minccino","Cinccino","Gothita","Gothorita","Gothitelle","Solosis","Duosion","Reuniclus","Ducklett","Swanna","Vanillite","Vanillish","Vanilluxe","Deerling","Sawsbuck","Emolga","Karrablast","Escavalier","Foongus","Amoonguss","Frillish","Jellicent","Alomomola","Joltik","Galvantula","Ferroseed","Ferrothorn","Klink","Klang","Klinklang","Tynamo","Eelektrik","Eelektross","Elgyem","Beheeyem","Litwick","Lampent","Chandelure","Axew","Fraxure","Haxorus","Cubchoo","Beartic","Cryogonal","Shelmet","Accelgor","Stunfisk","Mienfoo","Mienshao","Druddigon","Golett","Golurk","Pawniard","Bisharp","Bouffalant","Rufflet","Braviary","Vullaby","Mandibuzz","Heatmor","Durant","Deino","Zweilous","Hydreigon","Larvesta","Volcarona","Cobalion","Terrakion","Virizion","Tornadus","Thundurus","Reshiram","Zekrom","Landorus","Kyurem","Keldeo","Meloetta","Genesect","Chespin","Quilladin","Chesnaught","Fennekin","Braixen","Delphox","Froakie","Frogadier","Greninja","Bunnelby","Diggersby","Fletchling","Fletchinder","Talonflame","Scatterbug","Spewpa","Vivillon","Litleo","Pyroar","Flabebe","Floette","Florges","Skiddo","Gogoat","Pancham","Pangoro","Furfrou","Espurr","Meowstic","Honedge","Doublade","Aegislash","Spritzee","Aromatisse","Swirlix","Slurpuff","Inkay","Malamar","Binacle","Barbaracle","Skrelp","Dragalge","Clauncher","Clawitzer","Helioptile","Heliolisk","Tyrunt","Tyrantrum","Amaura","Aurorus","Sylveon","Hawlucha","Dedenne","Carbink","Goomy","Sliggoo","Goodra","Klefki","Phantump","Trevenant","Pumpkaboo","Gourgeist","Bergmite","Avalugg","Noibat","Noivern","Xerneas","Yveltal","Zygarde","Diancie","Hoopa","Volcanion","Rowlet","Dartrix","Decidueye","Litten","Torracat","Incineroar","Popplio","Brionne","Primarina","Pikipek","Trumbeak","Toucannon","Yungoos","Gumshoos","Grubbin","Charjabug","Vikavolt","Crabrawler","Crabominable","Oricorio","Cutiefly","Ribombee","Rockruff","Lycanroc","Wishiwashi","Mareanie","Toxapex","Mudbray","Mudsdale","Dewpider","Araquanid","Fomantis","Lurantis","Morelull","Shiinotic","Salandit","Salazzle","Stufful","Bewear","Bounsweet","Steenee","Tsareena","Comfey","Oranguru","Passimian","Wimpod","Golisopod","Sandygast","Palossand","Pyukumuku","Type:Null","Silvally","Minior","Komala","Turtonator","Togedemaru","Mimikyu","Bruxish","Drampa","Dhelmise","Jangmo-o","Hakamo-o","Kommo-o","TapuKoko","TapuLele","TapuBulu","TapuFini","Cosmog","Cosmoem","Solgaleo","Lunala","Nihilego","Buzzwole","Pheromosa","Xurkitree","Celesteela","Kartana","Guzzlord","Necrozma","Magearna","Marshadow"};
    Context context;
    boolean doRemove;
    long lastRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        lastRefresh = 0;
        preferences = getSharedPreferences("settings", 0);
        context = this;
        super.onCreate(savedInstanceState);
        doRemove = preferences.getBoolean("removeSniped", false);
        setContentView(R.layout.activity_pokemon_feed);
        refreshFeed();
    }
    public void refreshFeed(){
        if(System.currentTimeMillis()/1000 - lastRefresh < 20){
            Toast.makeText(context, "Please wait " + (20 - (System.currentTimeMillis()/1000 - lastRefresh)) + " more seconds.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        Log.d("LastRefresh: ", " " + lastRefresh);
        final ArrayList<Pokemon> list = getFeedData();
        if(preferences.getBoolean("freshSpawns", false)) {
            lastRefresh = System.currentTimeMillis() / 1000L;
        }
        final PokemonFeedAdapter adapter = new PokemonFeedAdapter(list, this);
        final ListView listView = findViewById(R.id.feed_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pokemon current = list.get(position);
                if(preferences.getBoolean("supportAppninjas", false)){
                    Intent intent = new Intent(context, BackgroundService.class);
                    intent.setAction("snipeAppninjas");
                    intent.putExtra("x", current.getX());
                    intent.putExtra("y", current.getY());
                    intent.putExtra("name", current.getName());
                    intent.putExtra("id", current.getID());
                    startService(intent);
                }
                if(preferences.getBoolean("handleClipboard", false)){
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("Coordinates", (Double.toString(current.getX())) + ", " + Double.toString(current.getY()));
                    clipboard.setPrimaryClip(clip);
                }
                if(doRemove) {
                    list.remove(position);
                    adapter.notifyDataSetChanged();
                }
                if (preferences.getBoolean("autoLaunchGO", false)) {
                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.nianticlabs.pokemongo");
                    if (launchIntent != null) {
                        startActivity(launchIntent);
                    }
                }
            }
        });
    }

    public void refreshPress(View v){
        refreshFeed();
    }

    public ArrayList<Pokemon> getFeedData(){
        Log.d("Feed: ", "Getting feed Data.");
        String pokeIDs;
        String nycfeed;
        String sgfeed;
        JSONObject feedNYC;
        JSONObject feedSG;
        preferences = getSharedPreferences("settings", 0);
        pokeIDs = preferences.getString("feedIDs", "");
        JSONArray feedArrayNYC = new JSONArray();
        JSONArray feedArraySG = new JSONArray();
        if(preferences.getBoolean("nycFeed", false)){
            nycfeed = getFeed("http://nycpokemap.com/query2.php", pokeIDs);
            Log.d("NYCFeed: ", "Got feed from NYC.");
            try {
                feedNYC = new JSONObject(nycfeed);
                feedArrayNYC = feedNYC.getJSONArray("pokemons");
                Log.d("NYCFeed: ", "Added Feed to array.");
                Log.d("NYCFeed: ", feedArrayNYC.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(preferences.getBoolean("sgFeed", false)){
            sgfeed = getFeed("http://sgpokemap.com/query2.php", pokeIDs);
            Log.d("SGFeed: ", "Got feed from SG.");
            try {
                feedSG = new JSONObject(sgfeed);
                feedArraySG = feedSG.getJSONArray("pokemons");
                Log.d("SGFeed: ", "Got feed from SG.");
                Log.d("SGFeed: ", feedArraySG.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return(buildFeed(feedArrayNYC, feedArraySG));
    }
    protected String getFeed(String... in){
        String data;
        try {
            UrlHandler handler = new UrlHandler();
            handler.setLastRefresh(lastRefresh);
            data = handler.execute(in[0], in[1])
                    .get()[0];
        } catch(Exception e) {
            Log.d("ERROR", "Exception in LiveMapHandlerService.java" + e.toString());
            return "ERROR" + e.toString();
        }
        return data;
    }

    protected ArrayList<Pokemon> buildFeed(JSONArray... dataArray){
        ArrayList<Pokemon> adapterArray = new ArrayList<>();
        for(int i = 0; i < dataArray.length; i++) {
            for (int j = 0; j < dataArray[i].length(); j++) {
                try {
                    JSONObject object = dataArray[i].getJSONObject(j);
                    //"pokemon_id":"1","lat":"40.60911699","lng":"-74.02313399","despawn":"1552994269","disguise":"0","attack":"-1","defence":"-1"
                    // ,"stamina":"-1","move1":"-1","move2":"-1","costume":"-1","gender":"1","shiny":"0","form":"-1","cp":"-1","level":"-1","weather":"1"
                    String name = pokeNames[object.getInt("pokemon_id") - 1];
                    int id = object.getInt("pokemon_id");
                    double x = object.getDouble("lat");
                    double y = object.getDouble("lng");
                    long despawn = object.getLong("despawn");
                    boolean shiny = (object.getInt("shiny") != 0);
                    Pokemon pokemon = new Pokemon(name, id, x, y, despawn, shiny);
                    adapterArray.add(pokemon);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return adapterArray;
    }

}
