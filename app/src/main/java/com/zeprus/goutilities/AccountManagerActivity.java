package com.zeprus.goutilities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import eu.chainfire.libsuperuser.Shell;

public class AccountManagerActivity extends AppCompatActivity{
    ArrayList<Account> accounts = new ArrayList<>();
    File accountDir;
    AccountAdapter adapter;
    ListView listView;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context context;
    private PackageManager pm;
    private int goUID;
    ActivityManager am;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences("settings", 0);
        editor = prefs.edit();
        context = this;
        setContentView(R.layout.activity_account_manager);
        accountDir = new File(getFilesDir().toString() + "/accounts");
        listView = findViewById(R.id.account_list);
        pm = context.getPackageManager();
        am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        ApplicationInfo appInfo = null;
        try {
            appInfo = pm.getApplicationInfo("com.nianticlabs.pokemongo", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert appInfo != null;
        goUID = appInfo.uid;
        refreshList();
        adapter = new AccountAdapter(accounts, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(!accounts.get((int) id).isCurrent()) {
                    Shell.SU.run("am force-stop com.nianticlabs.pokemongo");
                    // ! We can also use clear-data, particularly if we see that lag is
                    // ! starting to build off while switching lots of accounts:
                    ////Shell.SU.run("adb shell pm clear com.nianticlabs.pokemongo");
                    if(!prefs.getString("currAccount", "NULL").equals("NULL")){
                        File oldDir = new File(accountDir, prefs.getString("currAccount", "NULL"));
                        oldDir.mkdir();
                        Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/databases " + oldDir.getAbsolutePath());
                        Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/files " + oldDir.getAbsolutePath());
                        Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/no_backup " + oldDir.getAbsolutePath());
                        Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/shared_prefs " + oldDir.getAbsolutePath());
                    }
                    String group = "u0_a" + (goUID - 10000), user = "u0_a" + (goUID - 10000);
                    File accountDir = new File(accounts.get((int) id).getDir());
                    File[] subDirs = accountDir.listFiles();
                    for (File subDir : subDirs) {
                        if (subDir.isDirectory()) {
                            Shell.SU.run("cp -R " + subDir.getAbsolutePath() + " /data/data/com.nianticlabs.pokemongo/");
                            Shell.SU.run("chown -R " + user + ":" + group + " /data/data/com.nianticlabs.pokemongo/" + subDir.getName());
                        }
                    }
                    editor.putString("currAccount", accountDir.getName());
                    editor.apply();
                    Toast toast = Toast.makeText(context, "Switched to account '" + accounts.get((int) id).getName() + "'.", Toast.LENGTH_SHORT);
                    toast.show();
                    refreshList();
                    adapter.notifyDataSetChanged();
                    Intent launchIntent = pm.getLaunchIntentForPackage("com.nianticlabs.pokemongo");
                    startActivity(launchIntent);
                }
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                view.setEnabled(false);
                Shell.SU.run("rm -r " + accounts.get((int) id).getDir());
                Toast toast = Toast.makeText(context, "Deleted account '" + accounts.get((int) id).getName() + "'.", Toast.LENGTH_SHORT);
                toast.show();
                accounts.remove((int) id);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }
    public void addAccount(View v){
        EditText input = findViewById(R.id.text_acm_name);
        if(input.getText().toString().equals("")){
            Toast.makeText(this, "Please enter the account name.", Toast.LENGTH_SHORT).show();
        } else {
            editor.putString("currAccount",input.getText().toString());
            editor.apply();
            File newDir = new File(accountDir, input.getText().toString());
            newDir.mkdir();
            Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/databases " + newDir.getAbsolutePath());
            Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/files " + newDir.getAbsolutePath());
            Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/no_backup " + newDir.getAbsolutePath());
            Shell.SU.run("cp -R /data/data/com.nianticlabs.pokemongo/shared_prefs " + newDir.getAbsolutePath());
            refreshList();
            adapter.notifyDataSetChanged();
        }
    }

    public void refreshList(){
        accounts.clear();
        File[] subDirs = accountDir.listFiles();
        for (File subDir: subDirs) {
            boolean current = false;
            if(subDir.isDirectory()) {
                if(subDir.getName().equals(prefs.getString("currAccount", "NULL"))){current = true;}
                accounts.add(new Account(subDir.getName(), subDir.getAbsolutePath(), current));
            }
        }
    }
}
