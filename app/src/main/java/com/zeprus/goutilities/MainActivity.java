package com.zeprus.goutilities;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.File;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PreferenceManager.setDefaultValues(this, "settings", 0, R.xml.default_settings, false);
        createNotificationChannel();
        preferences = getSharedPreferences("settings", 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        File accountDir = new File(getFilesDir(), "accounts");
        accountDir.mkdir();


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        if(!preferences.getBoolean("rootMode", false)){
            navigationView.getMenu().setGroupVisible(R.id.root, false);
        } else {
            navigationView.getMenu().setGroupVisible(R.id.root, true);
        }
        navigationView.setNavigationItemSelectedListener(this);

        boolean running = BackgroundService.isRunning();
        if(running){
            Button button = findViewById(R.id.button_toggle_service);
            button.setText("Stop Service");
        }
    }

    public void onResume(){
        super.onResume();
        NavigationView navigationView = findViewById(R.id.nav_view);
        if(!preferences.getBoolean("rootMode", false)){
            navigationView.getMenu().setGroupVisible(R.id.root, false);
        } else {
            navigationView.getMenu().setGroupVisible(R.id.root, true);
        }
        if(BackgroundService.isRunning()){
            Button button = findViewById(R.id.button_toggle_service);
            button.setText("Stop Service");
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_settings) {
            Intent i = new Intent(
              MainActivity.this,
              SettingsActivity.class
            );
            startActivity(i);
        }
        if (id == R.id.menu_about) {
            Intent i = new Intent(
                MainActivity.this,
                AboutActivity.class
            );
            startActivity(i);
        }
        if(id == R.id.menu_account_manager){
            Intent i = new Intent(
                    MainActivity.this,
                    AccountManagerActivity.class
            );
            startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void toggleService(View v){
        boolean running = BackgroundService.isRunning();
        if(!running) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(this, BackgroundService.class));
            } else {
                startService(new Intent(this, BackgroundService.class));
            }
            Button button = findViewById(R.id.button_toggle_service);
            button.setText("Stop Service");
        } else {
            stopService(new Intent(this, BackgroundService.class));
            Button button = findViewById(R.id.button_toggle_service);
            button.setText("Start Service");
        }
    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name_running);
            String description = getString(R.string.channel_description_running);
            NotificationChannel channel = new NotificationChannel("running", name, NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(description);
            String name2 = getString(R.string.channel_name_sniping);
            String description2 = getString(R.string.channel_description_sniping);
            NotificationChannel channel2 = new NotificationChannel("snipe", name2, NotificationManager.IMPORTANCE_HIGH);
            channel2.setDescription(description2);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            notificationManager.createNotificationChannel(channel2);
        }
    }
}
