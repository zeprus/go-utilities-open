package com.zeprus.goutilities;

public class Account {
public String name;
private String dir;
private boolean current;
public Account(String name, String dir, boolean current){
    this.name = name;
    this.dir = dir;
    this.current = current;
}

    public boolean isCurrent() {
        return current;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public String getDir() {
        return dir;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
