package com.zeprus.goutilities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PackageAdapter extends ArrayAdapter<AppPackage> {
    private ArrayList<AppPackage> packages;
    private Context context;

    PackageAdapter(ArrayList<AppPackage> packages, Context context){
        super(context, R.layout.pokemon_pref_list_template, packages);
        this.packages = packages;
        this.context = context;
    }

    private static class ViewHolder {
        TextView name;
        ImageView icon;
        TextView label;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return packages.size();
    }

    @Override
    public AppPackage getItem(int position) {
        return packages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        PackageAdapter.ViewHolder holder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.packages_list_template, parent, false);
            holder = new PackageAdapter.ViewHolder();
            holder.name = convertView.findViewById(R.id.packages_template_name);
            holder.icon = convertView.findViewById(R.id.packages_template_icon);
            holder.label = convertView.findViewById(R.id.packages_template_label);

            convertView.setTag(holder);
        }
        else
        {
            holder=(PackageAdapter.ViewHolder) convertView.getTag();
        }
        holder.name.setText(packages.get(position).packageName);
        holder.label.setText(packages.get(position).label);
        holder.icon.setImageDrawable(packages.get(position).icon);

        return convertView;

    }

}
