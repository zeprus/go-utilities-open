package com.zeprus.goutilities;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlHandler extends AsyncTask<String, Void, String[]> {
    final private Pattern patternCoords = Pattern.compile("(-?[0-9]+[.,][0-9]+\\s*[.,]\\s*-?[0-9]+[.,][0-9]+)");
    final private Pattern patternMon = Pattern.compile("(?!>)[0-9]+.[a-z,A-Z]+(?=<)");
    private String content[] = new String[2];
    private long lastRefresh = 0;

    public void setLastRefresh(long lastRefresh) {
        this.lastRefresh = lastRefresh;
    }

    @Override
    protected String[] doInBackground(String... in) {
        String[] result = new String[2];
        if (in[0].matches(".*api\\.pokedex100\\.com/discord/free.*")) {
            result = handleCommunityURL(in);
        }
        if (in[0].matches(".*query2\\.php.*")) {
            Log.d("LIVEMAP", "Chose livemap");
            result[0] = handleLiveMap(in[0], in[1]);
        }
        return result;
    }

    private String[] handleCommunityURL(String... in) {
        Log.d("handleCommunityURL:", "Handling commuity URL: " + in[0]);
        try {
            URL url = new URL("http://" + in[0]);
            String cookies = in[1];
            URLConnection connection = url.openConnection();
            connection.setDoOutput(false);
            connection.setRequestProperty("Cookie", cookies);
            connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            BufferedReader responseReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            );
            String line;
            while ((line = responseReader.readLine()) != null) {
                Log.d("Line:", line);
                if (line.matches(".*(?!>)[0-9]+.[a-z,A-Z]+(?=<).*")) {
                    Matcher match = patternMon.matcher(line);
                    if (match.find()) {
                        content[0] = match.group(0);
                    }
                } else if (line.matches(".*(-?[0-9]+[.,][0-9]+\\s*[.,]\\s*-?[0-9]+[.,][0-9]+).*")) {
                    Matcher match = patternCoords.matcher(line);
                    if (match.find()) {
                        content[1] = match.group(0);
                    }
                }
            }
            responseReader.close();
        } catch (Exception e) {
            content[0] = "ERROR";
            content[1] = e.toString();
            return content;
        }
        return content;
    }

    private String handleLiveMap(String... in) {
        Log.d("LIVEMAP", "Arrived in handleLiveMap");
        String content = "";
        try {
            URL url = new URL(in[0] + "?since=" + lastRefresh +"&mons=" + in[1]);
            URLConnection connection = url.openConnection();
            connection.setDoOutput(false);
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty("Referer", "https://nycpokemap.com/?forcerefresh");
            StringBuffer buffer = new StringBuffer();
            BufferedReader responseReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            );
            String line;
            while ((line = responseReader.readLine()) != null) {
                buffer.append(line);
            }
            responseReader.close();
            content = buffer.toString();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return content;
    }
}
