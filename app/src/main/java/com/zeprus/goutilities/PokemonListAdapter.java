package com.zeprus.goutilities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PokemonListAdapter extends ArrayAdapter<PokemonDisplay> implements View.OnClickListener{
    private ArrayList<PokemonDisplay> pokemonArrayList;
    private ArrayList<PokemonDisplay> orig;
    private Context context;

    private static class ViewHolder {
        TextView name;
        TextView number;
        LinearLayout layout;
    }

    PokemonListAdapter(ArrayList<PokemonDisplay> pokemonArrayList, Context context){
        super(context, R.layout.pokemon_pref_list_template, pokemonArrayList);
        this.pokemonArrayList = pokemonArrayList;
        this.context = context;
    }

    @NonNull
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<PokemonDisplay> results = new ArrayList<>();
                if (orig == null)
                    orig = pokemonArrayList;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final PokemonDisplay g : orig) {
                            if (g.getName().toLowerCase()
                                    .contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                pokemonArrayList = (ArrayList<PokemonDisplay>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return pokemonArrayList.size();
    }

    @Override
    public PokemonDisplay getItem(int position) {
        return pokemonArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onClick(View v) {
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.pokemon_pref_list_template, parent, false);
            holder = new ViewHolder();
            holder.name = convertView.findViewById(R.id.pokemon_pref_template_name);
            holder.number = convertView.findViewById(R.id.pokemon_pref_template_number);
            holder.layout = convertView.findViewById(R.id.pokemon_pref_template_layout);
            convertView.setTag(holder);
        }
        else
        {
            holder=(ViewHolder) convertView.getTag();
        }

        holder.name.setText(pokemonArrayList.get(position).getName());
        holder.number.setText(String.valueOf(pokemonArrayList.get(position).getId()));
        if(pokemonArrayList.get(position).isEnabled()){
            holder.layout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            holder.layout.setBackgroundColor(ContextCompat.getColor(context, R.color.windowBackground));
        }

        return convertView;

    }
}
