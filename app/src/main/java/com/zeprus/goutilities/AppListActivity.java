package com.zeprus.goutilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class AppListActivity extends AppCompatActivity {
    PackageManager pm;
    PackageAdapter adapter;
    ArrayList<AppPackage> applist;
    ListView listView;
    SharedPreferences prefs;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);
        listView = findViewById(R.id.app_list);
        applist = new ArrayList<>();
        pm = getPackageManager();
        prefs = getSharedPreferences("settings", 0);
        context = this;
        for(ApplicationInfo info : pm.getInstalledApplications(PackageManager.GET_META_DATA)){
            applist.add(new AppPackage(info.packageName, info.loadIcon(pm), info.loadLabel(pm).toString()));
        }
        adapter = new PackageAdapter(applist, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prefs.edit().putString("packageNameAppninjas", applist.get((int) id).packageName)
                        .apply();
                Toast.makeText(context, "Submitted " + applist.get((int) id).packageName, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }
}
