package com.zeprus.goutilities;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BackgroundService extends IntentService {
    public static boolean running;
    public static boolean isRunning(){
        return running;
    }
    public BackgroundService(){
        super("BackgroundService");
    }
    ClipboardManager clipboard;
    ClipboardManager.OnPrimaryClipChangedListener clipboardListener;
    NotificationManager manager;
    SharedPreferences preferences;
    String packagename;
    @Override
    protected void onHandleIntent(Intent intent){
        if(intent.getAction() != null) {
            if (intent.getAction().equals("snipeAppninjas")) {
                snipeAppninjas(
                        "" + intent.getDoubleExtra("x", 0) + ", " + intent.getDoubleExtra("y", 0),
                        intent.getStringExtra("name"),
                        packagename
                        );
            }
        }
    }
    public void onCreate(){
        preferences = getSharedPreferences("settings", 0);
        Intent mainIntent = new Intent(this, MainActivity.class);
        Intent feedIntent = new Intent(this, PokemonFeed.class);
        PendingIntent pendingMainIntent = PendingIntent.getActivity(this, 0, mainIntent, 0);
        PendingIntent pendingfeedIntent = PendingIntent.getActivity(this, 0, feedIntent, 0);
        NotificationCompat.Builder notificationBuilder  = new NotificationCompat.Builder(this, "running")
                .setContentTitle("GO Utilities is running")
                .setSmallIcon(R.drawable.notification)
                .setOngoing(true)
                .setContentIntent(pendingMainIntent);
        if(preferences.getBoolean("liveFeed", false)){
            notificationBuilder.addAction(R.drawable.notification, "Live Feed", pendingfeedIntent);
        }
        Notification notification = notificationBuilder.build();
        startForeground(1, notification);
        super.onCreate();
        running = true;
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
        final boolean handleCoords = preferences.getBoolean("supportCoordinates", false);
        final boolean handleComLinks = preferences.getBoolean("supportCommunityURI", false);
        final boolean supportAppninjas = preferences.getBoolean("supportAppninjas", false);
        final boolean handleClipboard = preferences.getBoolean("handleClipboard", false);
        final boolean autoTeleport = preferences.getBoolean("supportAutoTeleport", false);
        final boolean autoLaunchGO = preferences.getBoolean("autoLaunchGO", false);
        packagename = preferences.getString("packageNameAppninjas", "unset");
        final String discordCookies = preferences.getString("discordCookies", "unset");
        final Pattern patternComLinks = Pattern.compile("api\\.pokedex100\\.com/discord/free.*");
        final Pattern patternCoords = Pattern.compile(getString(R.string.regex_coords));

        if(handleClipboard | autoTeleport) {
            clipboardListener = new ClipboardManager.OnPrimaryClipChangedListener() {
                @Override
                public void onPrimaryClipChanged() {
                    boolean found = false;
                    Log.d("CLIP", "changed");
                    if (clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                        ClipData clipData = clipboard.getPrimaryClip();
                        ClipData.Item content = clipData.getItemAt(0);
                        String out = content.toString();
                        String mon = "Missing No.";
                        if (handleCoords) {
                            Matcher match = patternCoords.matcher(out);
                            if (match.find()) {
                                out = match.group(1);
                                found = true;
                            }
                        }
                        assert discordCookies != null;
                        if (handleComLinks && !discordCookies.equals("unset")) {
                            Matcher match = patternComLinks.matcher(out);
                            if (match.find()) {
                                Log.d("communitylink: ", "Community link found.");
                                String params[] = new String[2];
                                params[0] = match.group();
                                params[1] = discordCookies;
                                found = true;
                                try {
                                    String result[] = new UrlHandler()
                                            .execute(params)
                                            .get();
                                    if (!result[0].equals("ERROR")) {
                                        out = result[1];
                                        mon = result[0];
                                    } else {
                                        Log.d("Error", result[1]);
                                    }
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (found) {
                            if (supportAppninjas) {
                                snipeAppninjas(out, mon, packagename);
                            }
                            if (handleClipboard) {
                                clipboard.removePrimaryClipChangedListener(clipboardListener);
                                clipboard.setPrimaryClip(ClipData.newPlainText("Coords", out));
                                clipboard.addPrimaryClipChangedListener(clipboardListener);
                            }
                            if (autoLaunchGO) {
                                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.nianticlabs.pokemongo");
                                if (launchIntent != null) {
                                    startActivity(launchIntent);
                                }
                            }
                        }
                    }
                }
            };
            clipboard.addPrimaryClipChangedListener(clipboardListener);
        }

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        if(intent != null) {
            onHandleIntent(intent);
        }
        return flags;
    }

    public void onDestroy(){
        running = false;
        clipboard.removePrimaryClipChangedListener(clipboardListener);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(0);
    }

    public void snipeAppninjas(String coords, String monster, String packagename){
        ArrayList<String> matches = new ArrayList<>();
        Matcher matcher = Pattern.compile("(-?[0-9]+)([,.])([0-9]+)")
                .matcher(coords);
        while(matcher.find()){
            matches.add(matcher.group());
        }
        String latitude = matches.get(0);
        String longitude = matches.get(1);
        double altitude = Math.random()*(180)+1;
        Log.d("coords","Lat: " + latitude + " Lng: " + longitude + " Alt: " + altitude);
        Intent intent = new Intent();
        intent.setAction("theappninjas.gpsjoystick.TELEPORT");
        intent.setPackage(packagename);
        intent.putExtra("lat", Float.parseFloat(latitude));
        intent.putExtra("lng", Float.parseFloat(longitude));
        intent.putExtra("alt", (float) altitude);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(intent);
        } else {
            this.startService(intent);
        }
        Notification notification  = new NotificationCompat.Builder(this, "snipe")
                .setContentTitle("Sniping: " + monster)
                .setContentText(coords)
                .setSmallIcon(R.drawable.crosshair)
                .setTimeoutAfter(2000)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();
        manager.notify(2, notification);
    }
}
