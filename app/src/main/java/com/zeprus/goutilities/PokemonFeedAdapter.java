package com.zeprus.goutilities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PokemonFeedAdapter extends ArrayAdapter<Pokemon> implements View.OnClickListener {
    private ArrayList<Pokemon> pokemonArrayList;
    private Context context;
    PokemonFeedAdapter(ArrayList<Pokemon> pokemonArrayList, Context context){
        super(context, R.layout.pokemon_pref_list_template, pokemonArrayList);
        this.pokemonArrayList = pokemonArrayList;
        this.context = context;
    }
    private static class ViewHolder {
        TextView name;
        TextView id;
        TextView coord;
        TextView iv;
        TextView cp;
        LinearLayout layout;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return pokemonArrayList.size();
    }

    @Override
    public Pokemon getItem(int position) {
        return pokemonArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onClick(View v) { }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        PokemonFeedAdapter.ViewHolder holder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.pokemon_feed_list_template, parent, false);
            holder = new PokemonFeedAdapter.ViewHolder();
            holder.name = convertView.findViewById(R.id.pokemon_feed_name);
            holder.id = convertView.findViewById(R.id.pokemon_feed_id);
            holder.coord = convertView.findViewById(R.id.pokemon_feed_coord);
            holder.cp = convertView.findViewById(R.id.pokemon_feed_cp);
            holder.iv = convertView.findViewById(R.id.pokemon_feed_iv);
            holder.layout = convertView.findViewById(R.id.pokemon_feed_layout);
            convertView.setTag(holder);
        }
        else
        {
            holder=(PokemonFeedAdapter.ViewHolder) convertView.getTag();
        }
        holder.name.setText(pokemonArrayList.get(position).getName());
        holder.id.setText(Integer.toString(pokemonArrayList.get(position).getPokemon_id()));
        holder.coord.setText(Double.toString(pokemonArrayList.get(position).getX()) + ", " + Double.toString(pokemonArrayList.get(position).getY()));
        if(pokemonArrayList.get(position).getCp() != 0) {
            holder.cp.setText(Integer.toString(pokemonArrayList.get(position).getCp()));
        }
        if(pokemonArrayList.get(position).getIv() != 0) {
            holder.iv.setText(Integer.toString(pokemonArrayList.get(position).getIv()));
        }
        return convertView;

    }
}
