package com.zeprus.goutilities;

import android.graphics.drawable.Drawable;

public class AppPackage {
    public String packageName;
    public Drawable icon;
    public String label;

    public AppPackage(String packageName, Drawable icon, String label){
        this.packageName = packageName;
        this.icon = icon;
        this.label = label;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String getLabel() {
        return label;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

}
